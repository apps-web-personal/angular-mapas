import { Component, OnInit } from '@angular/core';
import { Marcador } from '../../classes/marcador.class';

import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import { MapaEditarComponent } from './mapa-editar.component';
import { GelolocalizacionService } from '../../services/geolocalizacion/gelolocalizacion.service';

import Swal from 'sweetalert2';

declare const google: any;

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

  marcadores: Marcador[] = [];

  icon = {
    url: '../../../assets/icons/placeholder.svg',
    scaledSize: {
      width: 37,
      height: 45
    }
  }

  styles = {
    default: null,
    hide: [
      {
        // featureType: 'poi.business',
        featureType: 'poi',
        stylers: [{visibility: 'off'}]
      },
      {
        featureType: 'transit',
        elementType: 'labels.icon',
        stylers: [{visibility: 'off'}]
      },
      {
        featureType: 'administrative',
        stylers: [{visibility: 'off'}]
      }
    ]
  };

  lat_ini: number;
  lng_ini: number;

  // PARA CERRAR MARKERS AL ABRIR OTRO
  infoWindowOpened = null
  previous_info_window = null

  // GEOLOCALIZACION
  coordinadas_actual: { lat:number, lng:number};

  map: any;
  searchBox: any;
  autocomplete: any;

  public customStyle = [{  
    "featureType": "poi.medical",  
    "elementType": "all",  
    "stylers": [{  
        visibility: "off",  
    }]  
}, ]; 

  constructor( private _snackBar: MatSnackBar,
               public dialog: MatDialog,
               public _geoLocationService: GelolocalizacionService ) { 
      
    if( localStorage.getItem('marcadores') ){
      
      this.marcadores = JSON.parse(localStorage.getItem('marcadores'));
      
    }
  }

  ngOnInit() {

    this.modalCargando("Cargando Tiendas");

    //this.lat_ini = -12.243221453700581;
    //this.lng_ini = -76.92606596400651;

     this._geoLocationService.getPosition().subscribe(
       (pos: Position) => {
           this.coordinadas_actual = {
             lat:  +(pos.coords.latitude),
             lng: +(pos.coords.longitude)
           };
           console.log('coordinadas_actual',this.coordinadas_actual);
           this.lat_ini = this.coordinadas_actual.lat;
           this.lng_ini = this.coordinadas_actual.lng;
       });
  }

  agregarMarcador( evento ){

    const coords: { lat:number, lng:number} = evento.coords;
    console.log('coords',coords);

    const nuevoMarcador = new Marcador(coords.lat, coords.lng);
    this.marcadores.push(nuevoMarcador);
    this.guardarStorage();

    this._snackBar.open('Marcador agregado','Cerrar', {duration:3000});
    
  }

  borrarMarcador( i: number ){
      
    this.marcadores.splice(i, 1);
    this.guardarStorage();
    this._snackBar.open('Marcador borrado','Cerrar', {duration:3000});
  }

  guardarStorage(){

    localStorage.setItem('marcadores', JSON.stringify(this.marcadores));

  }

  editarMarcador( marcador: Marcador ){

    const dialogRef = this.dialog.open( MapaEditarComponent , {
      width: '250px',
      data: { titulo: marcador.titulo, desc: marcador.desc }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      if( !result ){
        return;
      }

      marcador.titulo = result.titulo;
      marcador.desc = result.desc;
      
      this.guardarStorage();
      this._snackBar.open('Marcador actualizado','Cerrar', {duration:3000});

    });

  }


  //PARA CERRAR UN MARCADOR AL SELECCIONAR OTRO
  close_window(){
    if (this.previous_info_window != null ) {
      this.previous_info_window.close()
    }    
  }
    
  select_marker(infoWindow){
    if (this.previous_info_window == null){
      this.previous_info_window = infoWindow;
    }
    else{
      this.infoWindowOpened = infoWindow
      try {
        this.previous_info_window.close()
      } catch (error) {
        // console.log('error',error);
      } 
    }
    this.previous_info_window = infoWindow
  }

  // AL CARGAR EL MAPA
  mapReady(event: any) {

      this.map = event;

      const input = document.getElementById('Map-Search');
      const styleControl = document.getElementById('style-selector-control');
      const myPosition = document.getElementById('myPosition');
 
      const input2 = document.getElementById('pac-input');

      // this.searchBox = new google.maps.places.SearchBox(input);
      // this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(document.getElementById('Settings'));
      // this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(document.getElementById('Profile'));
      // this.map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);
      
      this.map.controls[google.maps.ControlPosition.TOP_RIGHT].push(styleControl);
      this.map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(myPosition);

      // this.map.controls[google.maps.ControlPosition.TOP_RIGHT].push(document.getElementById('Logout'));
      // this.searchBox.addListener('places_changed', () => this.goToSearchedPlace());


    /*
      var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: this.map,
          anchorPoint: new google.maps.Point(0, -29)
        });*/

    
      this.autocomplete = new google.maps.places.Autocomplete(input2);
      // Set initial restrict to the greater list of countries.
      this.autocomplete.setComponentRestrictions({'country': ['pe']});
      // Specify only the data fields that are needed.
      this.autocomplete.setFields(
        ['address_components', 'geometry', 'icon', 'name']);

      this.autocomplete.addListener('place_changed', () => this.goToSearchedPlace());

      
        //this.autocomplete.addListener('place_changed', function() {
          /*infowindow.close();
          marker.setVisible(false);*/
          

          /*infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(this.map, marker);*/
        //});


  
      this.map.setOptions({styles: this.styles['default']});
      // this.map.setOptions({styles: this.styles['hide']});
  }

  ocultarLeyenda(checked:boolean){
    //var target = event.target;
    if (checked) {
      this.map.setOptions({styles: this.styles['hide']});
    } else {
      this.map.setOptions({styles: this.styles['default']});
    }
  }

  // goToSearchedPlace() {
  //     const places = this.searchBox.getPlaces();
  //     if (places.length === 0) {
  //     return;
  //     }
  //     const bounds = new google.maps.LatLngBounds();
  //     places.forEach((place) => {
  //     if (place.geometry.viewport) {
  //         bounds.union(place.geometry.viewport);
  //     } else {
  //         bounds.extend(place.geometry.location);
  //     }
  //     });
  //     this.map.fitBounds(bounds);
  // }

  goToSearchedPlace() {
    const place = this.autocomplete.getPlace();
    console.log('place',place);
    if (!place.geometry) {
      // User entered the name of a Place that was not suggested and
      // pressed the Enter key, or the Place Details request failed.
      console.log("No details available for input: '" + place.name + "'");
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      this.map.fitBounds(place.geometry.viewport);
    } else {
      this.map.setCenter(place.geometry.location);
      this.map.setZoom(17);  // Why 17? Because it looks good.
    }
    /*marker.setPosition(place.geometry.location);
    marker.setVisible(true);*/

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }
}

  irUbicacionActual(){

    console.log('MIS COORS AHORITA');

    this.map.setCenter({lat: this.lat_ini, lng: this.lng_ini}); 
    this.map.setZoom(18);

    // this._geoLocationService.getPosition().subscribe(
    //   (pos: Position) => {
    //       this.coordinadas_actual = {
    //         lat:  +(pos.coords.latitude),
    //         lng: +(pos.coords.longitude)
    //       };
    //       console.log('coordinadas_actual',this.coordinadas_actual);
    //       // this.lat_ini = this.coordinadas_actual.lat;
    //       // this.lng_ini = this.coordinadas_actual.lng;
          
    //       let myLatLng = new google.maps.LatLng({lat: this.lat_ini, lng: this.lng_ini}); 
    //       this.map.setCenter(myLatLng); 
    //   },
    //   error =>  {
    //     this.map.setCenter({lat: -12.243221453700581, lng: -76.92606596400651});
    //   }
    //   );
  }

  modalCargando( titulo: string, mensaje: string = 'Por favor, espere' ){

    Swal.fire({
      title: titulo,
      text: mensaje,
      icon: 'info',
      timer: 2500,
      allowOutsideClick: false
    });
    return Swal.showLoading();

  }

}
