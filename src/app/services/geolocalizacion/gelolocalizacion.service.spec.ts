import { TestBed } from '@angular/core/testing';

import { GelolocalizacionService } from './gelolocalizacion.service';

describe('GelolocalizacionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GelolocalizacionService = TestBed.get(GelolocalizacionService);
    expect(service).toBeTruthy();
  });
});
